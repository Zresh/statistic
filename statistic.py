import logging
from pony.orm import *
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

db = Database('sqlite', 'qwertyu.sqlite', create_db=True)

CHANNEL_NAME = '@cataway'


class Statistic(db.Entity):
    id = PrimaryKey(int, auto=True)
    userid = Required(int, unique=True)
    firstName = Required(str)
    lastName = Required(str)
    userName = Required(str)
    city = Optional(str)
    ismen = Optional(str)
    phone = Optional(str)
    score = Required(int)
    age = Optional(int)
    isrepost = Required(int)
    islike = Required(int)

show(Statistic)


db.generate_mapping(create_tables=True)


def start(bot, update, message):
    stat = [InlineKeyboardButton("Like", callback_data='1')]
    reply_markup = InlineKeyboardMarkup(stat)
    bot.send_message(message.chat.id, text='If u wanna see stat press da button', reply_markup=reply_markup)


def button(bot, update, message):
    query = update.callback_query
    with db_session:
        user_id = "{}".format(query.from_user.id)
        select(user_id.score for user_id in Statistic if user_id.score != 0)
        select(user_id.score for user_id in Statistic if user_id.ismen != 0)
        select(user_id.score for user_id in Statistic if user_id.age != 0)
        select(user_id.score for user_id in Statistic if user_id.city != 0)

def main():
    updater = Updater("499636691:AAFLcr3zl0mc-c_P9-cbxO9JNcMbPwI4Qdw")

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CallbackQueryHandler(button))
    updater.start_polling()

    updater.idle()


with db_session:
    Statistic.select().show()


if __name__ == '__main__':
    main()